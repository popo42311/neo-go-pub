package network

import (
	"github.com/cobinhood/neo-go/pkg/network/payload"
	"github.com/cobinhood/neo-go/pkg/util"
)

type Peer interface {
	Endpoint() util.Endpoint
	Disconnect(error)
	Send(msg *Message)
	Done() chan error
	Version() *payload.Version
}
