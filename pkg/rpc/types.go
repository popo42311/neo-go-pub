package rpc

type InvokeScriptResponse struct {
	responseHeader
	Result *InvokeResult
}

// InvokeResult represents the outcome of a script that is
// executed by the NEO VM.
type InvokeResult struct {
	State       string `json:"state"`
	GasConsumed string `json:"gas_consumed"`
	Stack       []*StackParam
}

// StackParam respresent a stack parameter.
type StackParam struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

// AccountStateResponse holds the getaccountstate response.
type AccountStateResponse struct {
	responseHeader
	Result *Account `json:"result"`
}

// Account respresents details about a NEO account.
type Account struct {
	Version    int    `json:"version"`
	ScriptHash string `json:"script_hash"`
	Frozen     bool
	// TODO: need to check this field out.
	Votes    []interface{}
	Balances []*Balance
}

// Balance respresents details about a NEO account balance.
type Balance struct {
	Asset string `json:"asset"`
	Value string `json:"value"`
}

type params struct {
	values []interface{}
}

func newParams(vals ...interface{}) params {
	p := params{}
	p.values = make([]interface{}, len(vals))
	for i := 0; i < len(p.values); i++ {
		p.values[i] = vals[i]
	}
	return p
}

type request struct {
	JSONRPC string        `json:"jsonrpc"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
	ID      int           `json:"id"`
}

type responseHeader struct {
	ID      int    `json:"id"`
	JSONRPC string `json:"jsonrpc"`
}

type response struct {
	responseHeader
	Result interface{} `json:"result"`
}

type GetBlockResp struct {
	responseHeader
	Result Block `json:"result"`
}

type ValidateAddressResp struct {
	responseHeader
	Result struct {
		Address string `json:"address"`
		Isvalid bool   `json:"isvalid"`
	} `json:"result"`
}

type GetRawTransactionResp struct {
	responseHeader
	Result BlockTX `json:"result"`
}

type BlockTX struct {
	TXID string `json:"txid"`
	Type string `json:"type"`
	Vin  []struct {
		TXID  string `json:"txid"`
		Index int64  `json:"vout"`
	} `json:"vin"`
	Vout []struct {
		N       int64  `json:"n"`
		Asset   string `json:"asset"`
		Value   string `json:"value"`
		Address string `json:"address"`
	} `json:"vout"`
	Scripts []struct {
		Invocation   string `json:"invocation"`
		Verification string `json:"verification"`
	}
	Confirmations int64 `json:"confirmations"`
}

type Block struct {
	Hash  string    `json:"hash"`
	Index int64     `json:"index"`
	Time  int64     `json:"time"`
	Tx    []BlockTX `json:"tx"`
}
