package main

import (
	"os"

	"github.com/cobinhood/neo-go/cli/server"
	"github.com/cobinhood/neo-go/cli/smartcontract"
	"github.com/cobinhood/neo-go/cli/wallet"
	"github.com/urfave/cli"
)

func main() {
	ctl := cli.NewApp()
	ctl.Name = "neo-go"
	ctl.Usage = "Official Go client for Neo"

	ctl.Commands = []cli.Command{
		server.NewCommand(),
		smartcontract.NewCommand(),
		wallet.NewCommand(),
	}

	ctl.Run(os.Args)
}
